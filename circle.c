
#include<stdio.h>
#define PI 3.14
int input()
{
	int r;
	printf("enter radius value\n");
	scanf("%d", &r);
	return r;
}

float compute_area(int r)
{
	float cir;
	cir = 2 * PI * r;
	return cir;
}

float compute_circumference(int r)
{
	float ar;
	ar = PI * r * r;
	return ar;
}

void output(float cir, float ar)
{
	printf("the circumference of the given circle is %f\n", cir);
	printf("the area of the given circle is %f\n", ar);
}

int main()
{
	int in;
	float area, circumference;
	int out;
	in = input();
	area = compute_area(in);
	circumference = compute_circumference(in);
	output(area, circumference);
	return 0;
}