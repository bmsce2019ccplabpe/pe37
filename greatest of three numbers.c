#include<stdio.h>
int
input ()
{
  int a;
  scanf ("%d", &a);
  return a;
}

int
compute (int a, int b, int c)
{
  if (a > b && a > c)
    return a;
  else if (b > a && b > c)
    return b;
  else
    return c;
}

void
output (int g)
{
  printf ("the greatest of three numbers is %d\n", g);
}

int
main ()
{
  int a, b, c, res;
  printf ("enter three numbers\n");
  a = input ();
  b = input ();
  c = input ();
  res = compute (a, b, c);
  output (res);
  return 0;