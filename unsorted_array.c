#include<stdio.h>
int input (int a[])
{
  int n, i;
  printf ("enter the size of the array\n");
  scanf ("%d", &n);
  printf ("enter the elements of the array\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  return n;
}

void compute (int n, int a[])
{
  int i, sm, la, sm_pos, la_pos, temp;
  la = a[0];
  sm = a[0];
  la_pos = 0;
  sm_pos = 0;
  for (i = 1; i < n; i++)
    {
      if (a[i] < sm)
	{
	  sm = a[i];
	  sm_pos = i;
	}
      if (a[i] > la)
	{
	  la = a[i];
	  la_pos = i;
	}
    }
  printf ("smallest number in the array is %d an its position is %d\n", sm,
	  sm_pos);
  printf ("largest number in the array is %d and its position is %d\n", la,
	  la_pos);
  temp = a[la_pos];
  a[la_pos] = a[sm_pos];
  a[sm_pos] = temp;
}

void output (int n, int a[])
{
  int i;
  printf ("the new array is\n");
  for (i = 0; i < n; i++)
    printf ("%d\n", a[i]);
}

int main ()
{
  int a[20], x;
  x = input (a);
  compute (x, a);
  output (x, a);
  return 0;
}